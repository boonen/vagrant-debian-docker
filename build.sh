#!/bin/bash
# use -f to force (re)creation of a box

if [ "$#" -gt 0 ] && [ "$@" == "-f" ]
  then
    vagrant destroy -f
    rm -rf .vagrant *.box
fi

vagrant up

vagrant ssh -c "sudo touch /etc/apt/sources.list.d/debian-backports.list"
vagrant ssh -c "sudo touch /etc/apt/sources.list.d/docker.list"
vagrant ssh -c "echo 'deb http://http.debian.net/debian jessie-backports main' | sudo tee --append /etc/apt/sources.list.d/debian-backports.list > /dev/null"
vagrant ssh -c "echo 'deb-src http://http.debian.net/debian jessie-backports main' | sudo tee --append /etc/apt/sources.list.d/debian-backports.list > /dev/null"
vagrant ssh -c "sudo sed -i '/^GRUB_CMDLINE_LINUX=/s/=.*/=\"debian-installer=en_US cgroup_enable=memory swapaccount=1\"/' /etc/default/grub"

vagrant ssh -c "sudo apt-key update && sudo apt-get update && apt-get dist-upgrade -y && sudo apt-get upgrade -y && sudo apt-get autoremove -y"
vagrant ssh -c "sudo apt-get install --no-install-recommends unzip curl apt-transport-https ca-certificates -y"
vagrant ssh -c "sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D"
vagrant ssh -c "echo 'deb https://apt.dockerproject.org/repo debian-jessie main' | sudo tee --append /etc/apt/sources.list.d/docker.list > /dev/null"
vagrant ssh -c "sudo apt-get update && sudo apt-get upgrade -y && sudo apt-get autoremove -y"
vagrant ssh -c "sudo apt-get install --no-install-recommends -t jessie-backports linux-image-amd64 -y && sudo apt-get autoremove -y"

vagrant reload

vagrant ssh -c "sudo apt-get install --no-install-recommends docker-engine -y"
vagrant ssh -c "sudo sed -i '/^ExecStart=/s/=.*/=\/usr\/bin\/dockerd -H fd:\/\/ --storage-driver=overlay2 --icc=false --iptables=true/' /lib/systemd/system/docker.service"
vagrant ssh -c "sudo systemctl daemon-reload && sudo systemctl restart docker"

vagrant ssh -c "sudo apt-get purge perl aptitude-doc-en doc-debian docutils-doc -y && sudo find /var/log -type f -delete"
vagrant ssh -c "sudo aptitude purge ~c -y && sudo apt-get clean && sudo apt-get autoclean"
vagrant ssh -c "sudo dd if=/dev/zero of=/EMPTY bs=1M"
vagrant ssh -c "sudo rm -f /EMPTY"
vagrant ssh -c "cat /dev/null > ~/.bash_history && history -c && exit"

vagrant halt
vagrant package --output debian-jessie-docker.box

curl 'https://atlas.hashicorp.com/api/v1/box/boonen/docker_debian-jessie64/version/8.6.1-1.12.3/provider/virtualbox/upload?access_token=TOKEN'

echo "Now use 'curl -X PUT --upload-file debian-jessie-docker.box https://binstore.hashicorp.com/TOKEN' to upload the file."